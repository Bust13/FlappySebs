using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Movimiento : MonoBehaviour
{
    public float FuerzaSalto = 10f;
    private Rigidbody2D rb;
    private PuntuacionManager puntuacionManager;
    private bool GameOver = false;
    public GameObject restart;
    

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        puntuacionManager = FindObjectOfType<PuntuacionManager>();
        restart.SetActive(false);
        Time.timeScale = 1;
    }


    private void Update()
    {
        
        if (GameOver && (Input.GetKeyDown(KeyCode.R))) 
        {
            Restart();
        }
    }

    public void Salto()
    {
        rb.velocity = Vector2.up * FuerzaSalto;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!GameOver)
        {
            Debug.Log("GameOver");
            puntuacionManager.StopScoring();
            restart.SetActive(true);
            GameOver = true;
            Time.timeScale = 0;
        }

    }

    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
