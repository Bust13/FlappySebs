using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class SpawnerTubos : MonoBehaviour
{
    public GameObject TuboArribaPrefab;
    public GameObject TuboAbajoPrefab;
    public float tiempoSpawn = 2f;
    public float offset = 10f;
    private float minTubosEntreMedio = 7.25f;
    private float maxTubosEntreMeido = 8f;
    private float pantallaArriba;
    private float pantallaAbajo;

    private float timer;

    private void Start()
    {
        Vector2 bordes = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        pantallaArriba = bordes.y;
        pantallaAbajo = -bordes.y;
        SpawnTubos();
    }

    private void Update()
    {
        if( timer < tiempoSpawn)
        {
            timer += Time.deltaTime;
        }
        else
        {
            SpawnTubos();
            timer = 0;
        }
    }

    void SpawnTubos()
    {
        float tubosEntreMedio = Random.Range(minTubosEntreMedio, maxTubosEntreMeido);
        float minY = pantallaAbajo + tubosEntreMedio / 2;
        float maxY = pantallaArriba - tubosEntreMedio / 2;
        float tuboPosicionY = Random.Range(minY, maxY);


        GameObject tuboInf = Instantiate(TuboAbajoPrefab, new Vector3(transform.position.x, tuboPosicionY - tubosEntreMedio / 2, 0),Quaternion.identity);
        GameObject TuboSup= Instantiate(TuboArribaPrefab, new Vector3(transform.position.x, tuboPosicionY + tubosEntreMedio / 2, 0), Quaternion.identity);

        Destroy(tuboInf,10f);
        Destroy(TuboSup,10f);
    }
}
