using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntuacionTrigger : MonoBehaviour
{
    private PuntuacionManager scoreManager;

    private void Start()
    {
        scoreManager = FindObjectOfType<PuntuacionManager>(); 
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player")) 
        {
            scoreManager.IncrementarPuntaucion(1);
        }
    }
}
