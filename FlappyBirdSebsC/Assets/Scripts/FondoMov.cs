using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FondoMov : MonoBehaviour
{
    [SerializeField] private Vector2 VelocidadMov;
    private Vector2 offset;
    private Material material;


    private void Awake()
    {
        material= GetComponent<SpriteRenderer>().material;

    }

    private void  Update()
    {
        offset = VelocidadMov * Time.deltaTime;
        material.mainTextureOffset += offset;
    }
}
