using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuntuacionManager : MonoBehaviour
{
    public Text scoreText;
    private float score = 0f;
    private bool isGameActive = true;
    private void Start()
    {
        score = 0f;
        UpdateScore();
       
    }
  public void IncrementarPuntaucion(int amount)
    {
        if (isGameActive)
        {
            score += amount;
            UpdateScore();
        }
    }

    public void StopScoring()
    {
        isGameActive = false;
    }

    private void UpdateScore()
    {
        scoreText.text =  score.ToString();
    }
}
